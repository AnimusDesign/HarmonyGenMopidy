from mpd import MPDClient
from harmonygen_mopidy.db import *
import mpd
import socket
import os
import json


class Client:
    """
    Provides an abstraction on top of the Python
    MPD2 library. Wraps to check connection state
    before executing a MPD command. This is to provide
    a common connection across the entire application.
    The client is instaniated and available at the root name space.

    :todo: Add logging
    :todo: Further conditional logic to verify/restore connections
    """
    _client = None  # The MPD client library instaniated instance,
    _host = 'localhost'  # The MPD host to connect too.
    _port = 6600  # The MPD port to connect too.
    _connected = False  # Whether connect to MPD

    def __init__(self):
        """
        Sets the _client property to match an instaniated
        instance of the MPD client library.

        :return:
        """
        self._client = MPDClient()
        self._client.timeout = 10
        self.connect()

    def connect(self):
        """
        Wrap the connection in a try catch statement,
        the return is set in the _connected property,
        which other methods ustilize to determine the
        current state.

        The _host and _port property are updated to reflect
        settings pulled from the client database. The settings
        are adjustable via the Web Interface. The setting
        retrieval is abstracted to the db namespace.

        :return:
        """
        try:
            self._host = set_setting(sname='host', dvalue='localhost')['value']
            self._port = set_setting(sname='port', dvalue='6600')['value']
            print(
                "Connecting to MPD:\n\tHost: ",
                self._host,
                "\n\tPort: ",
                self._port)
            self._client.connect(self._host, self._port)
        except Exception as e:
            self._connected = False
        else:
            self._connected = True

    def wrap_command(self, methname, args=None):
        """

        :param methname: The MPD command to executed
        :param args: Any arguments that are required for the method call, these are unpacked and should be provided as an array.
        :return:
        """
        func = eval("self._client.%s" % methname)
        try:
            # Try a basic command if the connection is lost an exception is
            # raised.
            self._client.currentsong()
        except Exception as e:
            # This will catch all Exceptions, and set the connection to false.
            # Then tries to reconnect to the MPD host.
            self._connected = False
            self._client.connect(self._host, self._port)
        finally:
            # After trying to connect if an exception if raised.
            if self._connected is True:
                # Verify that the connection is indeed active.
                resp = func() if args is None else func(*args)
                return resp
            else:
                # If we are not connected return a dictionary that the connection is not
                # active
                return {"error": "MPD is not actively running"}

    def get_current(self):
        """
        A wrapper method that provides the current song.
        :return: The current strong dictionary structure.
        """
        curr = self.wrap_command("currentsong")
        return curr
