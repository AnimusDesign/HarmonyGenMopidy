from harmonygen_mopidy.utils import MPDClientWrapper
from harmonygen_mopidy.target.mopidy import add_track_to_mpd
from harmonygen_mopidy.seeds import AVAILABLE_SEEDS
from harmonygen_mopidy.db import SimilarArtist, SimilarArtistService, scoped_sessions
from harmonygen_mopidy.db.utils import get_or_create, add_origin_mapping
from datetime import datetime
from musicbrainz.db import Artist as MArtist
from sqlalchemy import or_
from functools import partial
import concurrent.futures
import logging
LOGGER = logging.getLogger(__name__)


def get_similar_artists_director(
    config,
    artist,
    lastfm_client,
    branch_depth=1
):
    """
    The wrapper function to get similar artist.

    This use futures to scan for multiple similar artists.
    It maintains a task list. While there are still tasks
    available it will add that to the future to execute.
    The future context is set to run four instances.

    Inside the while loop. It will check for any complete futures.
    It gathers the return data from the target function *get_similar_artists*.
    The response contains a list of tasks, and similar artists.
    Both items are amended to their respective lists.


    .. seealso:: `class:harmonygen_mopidy.lastfm.LastFM`
    .. seealso:: `func:harmonygen_mopidy.director.get_similar_artists`

    .. todo:: There is a max memory limit size over a branch size of 2.

    .. codeauthor:: Animus Null <sean@animus.design>

    :param config: An instance of the parsed configuration.
    :param artist: The mbid of the artist you wish to query.
    :param lastfm_client: An instance of the LastFM class.
    :param branch_depth: How deep do you want to go, see the *get_similar_artists* function.
    :returns: A list of similar artists.
    """
    tasks = [(artist, 1)]
    similar = []
    futures = {}
    with concurrent.futures.ThreadPoolExecutor(max_workers=4) as executor:
        with scoped_sessions() as sessions:
            session, musicbrainz_session = sessions
            while len(tasks) > 0 or True in [
                    f.running() for f in futures.keys()]:
                if len(tasks) > 0:
                    artist, current_depth = tasks.pop(0)
                    futures.update(
                        {
                            executor.submit(
                                get_similar_artists,
                                (artist,
                                 current_depth),
                                config,
                                lastfm_client,
                                session,
                                musicbrainz_session,
                                branch_depth): artist})
                for future in concurrent.futures.as_completed(futures):
                    job = futures[future]
                    try:
                        sub_tasks, sub_similar, db_obj = future.result()
                        LOGGER.debug("Trying to add returned database object.")
                        session.add(db_obj)
                        LOGGER.debug(
                            "Successfully retreived response from get_similar_artists for job {}.".format(job))
                        LOGGER.debug(
                            "Received {} additional tasks.".format(
                                len(sub_tasks)))
                        LOGGER.debug(
                            "Received {} additional similar artists.".format(
                                len(sub_similar)))
                        tasks.extend(sub_tasks)
                        similar.extend(sub_similar)
                    except Exception as exc:
                        LOGGER.error(
                            "Failed to retrieve similar artists for mbid: {}".format(job),
                            exc_info=True)
    return similar


def get_similar_artists(
    task,
    config,
    lastfm_client,
    session,
    musicbrainz_session,
    branch_depth=1,
    try_count=1,
    max_tries=3
):
    """
    Given a musicbrainz id, find similar artists.

    This takes in a *task*, which is made of the music brainz
    id and current depth. This will first hit the database to see
    if that artist has already been cached, it will use that object.
    There is a field for **last_scanned**, which is not used. The
    intent for that field is it will rescan stale entries.

    If an entry is not present in the database it will query LastFM,
    see to do notes. The entry from last fm will then be cached
    in the database.

    If the current depth, reference back to **task** is not equal
    to the max branch depth. We then add all the similar artists
    as tasks for the *get_similar_artists_director* to query for.

    We are keeping the objects light. They are just a list of music
    brainz IDs. We will then query the music brainz database directly
    for additional data as necessary.

    **See Also**

    :class:`harmonygen_mopidy.db.SimilarArtist`

    :class:`harmonygen_mopidy.db.SimilarArtistService`

    :func:`harmonygen_mopidy.director.get_similar_artists_director`

    .. codeauthor:: Animus Null <sean@animus.design>

    .. todo:: Add logic to check time stamp and determine if a cache refresh is needed.
    .. todo:: Add support for additional similarity services.
    .. todo:: Switch the similar return to a namedtuple.

    :param task: A tuple of musicbrainz id and the current depth level.
    :param config: An instance of ConfigParser.
    :param lastfm_client: An instance of the LastFM class.
    :param branch_depth: How many branches to nest out from the origin artist.
    :returns: A tuple containing a list of tasks, and similar artists.
    """
    artist, current_depth = task
    try:
        LOGGER.debug("Checking to see if artist is in similar cache.")
        loaded_get_or_create = partial(get_or_create,
                                       session,
                                       SimilarArtist,
                                       {"origin_service": SimilarArtistService.LastFM,
                                        "first_scanned": datetime.utcnow(),
                                           "last_scanned": datetime.utcnow(),
                                           "similar": []})
        similar = []
        tasks = []
        exists = loaded_get_or_create(id=artist)
        if len(exists.similar) == 0:
            LOGGER.debug(
                "Similar artist is not present in cache, querying LastFM")
            rsp = lastfm_client.artist_getsimilar({
                "mbid": artist})["similarartists"]["artist"]
            similar.extend(rsp)
            sub_similar = []
            for sartist in rsp:
                if "mbid" not in sartist:
                    continue
                sub_similar.append(loaded_get_or_create(id=sartist["mbid"]))
            exists.similar.extend(sub_similar)
        else:
            LOGGER.debug("Artist is in similar cache")
            mrsp = musicbrainz_session.query(MArtist).filter(or_(
                *[MArtist.gid == s.id for s in exists.similar])).all()
            for ma in mrsp:
                if mrsp:
                    similar.append({
                        "mbid": ma.id,
                        "name": ma.name
                    })
        if branch_depth > 1 and current_depth != branch_depth:
            LOGGER.debug(
                "Not at maximum branch depth, currently at {} out of {}".format(
                    current_depth, branch_depth))
            tasks.extend([(similar.id, current_depth + 1)
                          for similar in exists.similar])
            LOGGER.debug(
                "Finished adding new tasks, added {} additional tasks.".format(
                    len(tasks)))
        return tasks, similar, exists
    except Exception as e:
        LOGGER.error(
            "Encountered error, getting simlar artists: {}".format(
                e.__str__()), exc_info=True)
        if try_count != max_tries:
            sleep(int(config["HarmonyGen"]["exception_stepoff_time"]))
            return get_similar_artists(
                task,
                config,
                lastfm_client,
                session,
                musicbrainz_session,
                branch_depth=branch_depth,
                try_count=try_count + 1)
        else:
            raise e


def add_tracks(config, track_recommendations, lastfm_client):
    """
    Given a track try to add to the enabled targets.

    .. todo:: Implement Spotify target logic.

    This will try and add a track to the corresponding
    target service. A list of track recommendations is provided.
    The exact dictionary is provided below. But the items
    should be service agnostic. Using *MusicBrainz* as the
    backing data service.

    At this time only Mopidy is supported. But it is meant
    to easily add ancillary target services. Provided
    a robust and supportive api.

    Regardless of service this is our general flow.

    We search the target service for the name of the artist.
    Or collaboration of artists. Filtering the list to just
    the *targets* representation of a track. We do not want
    to add entire albums in one shot.

    Now with a list of entire tracks by the artist in question.
    We start consuming the list of track dictionaries. It acts
    more like a *FIFO* queue. We pop off the first index. Trying
    to add that to the target sevice.

    If the response from the addition to the target service is False.
    We move on to the next track. Until we exhaust all recommended
    tracks.

    The last item we do is add an origin mapping. The origin
    mapping links how we got to this artist. Trust me
    when you go from Dropkick Murphy's to Dolly Partion, you're
    a wee bit curious.

    .. code-block:: python
        :name: track_recommendations example

        {
            1234: {
                "artist": "ArtistName",
                "artist_mbid": "artist_uuid",
                "track": "Some Awesome Song",
                "track_mbid": "The track music brainz ID"
            }
        }

    .. seealso:: `class:harmonygen_mopidy.db.ArtistOrigin`

    Once all is said and done, we return a boolean.


    .. seealso:: `class:harmonygen_mopidy.lastfm.LastFM`

    .. codeauthor:: Animus Null <sean@animus.design>

    :param config: An instance of ConfigParser.
    :param track_recommendations: A list of possible tracks to add.
    :param lastfm_client: A LastFM Client.
    :returns: Boolean True on success false on error.
    """
    responses = []
    for origin_artist, tracks in track_recommendations.items():
        rsp = False
        artist = tracks[0]["artist"]
        LOGGER.debug("Searching for tracks by artist: {}".format(artist))
        with MPDClientWrapper() as client:
            artist_results = [
                f for f in filter(
                    lambda t: "spotify:track" in t["file"],
                    client.search(
                        "any",
                        tracks[0]["artist"]))]
        LOGGER.debug("Found {} tracks for artist {}".format(
            len(artist_results), artist))
        while rsp is False and len(tracks) > 1:
            curr_track = tracks.pop()
            LOGGER.debug("Attempting to add track: {}".format(curr_track))
            rsp = add_track_to_mpd(curr_track, artist_results)
            if rsp is True:
                with scoped_sessions() as sessions:
                    session, musicbrainz_session = sessions
                    add_origin_mapping(
                        origin_artist,
                        curr_track["artist_mbid"],
                        curr_track["artist"],
                        session,
                        musicbrainz_session)
        responses.append(rsp)
    return False if False in responses else True
