from harmonygen_mopidy.utils import MPDClientWrapper
from difflib import SequenceMatcher
import logging
LOGGER = logging.getLogger(__name__)


def add_track_to_mpd(track, artist_results):
    """

    .. code-block:: python
        :name: Track Example

        {
            "artist": something,
            "artist_mbid": something,
            "track": something,
            "track_mbid": something
        }

    .. codeauthor:: Animus Null <sean@animus.design>

    :param track: A python dictionary containing the track information.
    :param artist_results: The list of results from an MPD search for the specified artist.
    :returns: A boolean. True on successful addition otherwise false.
    """
    def probability(y): return (
        SequenceMatcher(
            None,
            track["track"],
            y).ratio() * 100)
    with MPDClientWrapper() as client:
        filter_rsp = [(probability(t["title"]), t)
                      for t in artist_results
                      if "title" in t and int(probability(t["title"]) >= 70)]
        if len(filter_rsp) < 1:
            LOGGER.warning("Failed to find any tracks for: {}".format(track))
            return False
        filter_rsp = sorted(filter_rsp, reverse=True, key=lambda k: k[0])
        LOGGER.debug(
            "Adding track: {} to playlist with a similarity of {}%".format(
                filter_rsp[0][1]["file"],
                filter_rsp[0][0]))
        client.add(filter_rsp[0][1]["file"])
        return True
