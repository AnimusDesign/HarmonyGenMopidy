"""
.. codeauthor:: Animus Null <sean@animus.design>
"""
import logging
import logging.handlers
import os
CONFIG_DIR = os.path.join(os.environ["HOME"], ".config", "harmonygen_mpd")
BASE_DIR = os.path.join(os.environ['HOME'], ".config", "harmonygen_mpd")
TEMPLATE_DIR = os.path.join(os.path.dirname(__file__), "templates")

__LOGGING_FORMAT = logging.Formatter(
    "[%(asctime)s][%(levelname)s][%(threadName)s][%(thread)d][%(module)s][%(funcName)s][%(lineno)d] %(message)s")
ch = logging.StreamHandler()
ch.setLevel(logging.DEBUG)
ch.setFormatter(__LOGGING_FORMAT)
if not os.path.isdir(CONFIG_DIR):
    print("Config directory not present, creating.")
    os.makedirs(CONFIG_DIR)
fh = logging.handlers.RotatingFileHandler(
    os.path.join(
        CONFIG_DIR,
        "harmonygen.log"),
    maxBytes=2500,
    backupCount=0)
fh.setLevel(logging.DEBUG)
fh.setFormatter(__LOGGING_FORMAT)
logging.basicConfig(handlers=[ch, fh], level=logging.DEBUG)
