from harmonygen_mopidy import BASE_DIR, TEMPLATE_DIR
from difflib import SequenceMatcher
from jinja2 import Template
from configparser import ConfigParser
from mpd import MPDClient
from random import randint
import random
import logging
import os
LOGGER = logging.getLogger(__name__)


class MPDClientWrapper(MPDClient):
    """
    A thin wrapper atop the MPD Client.
    This provides a context manager to
    access the MPD Client.

    .. codeauthor:: Animus Null <sean@animus.design>
    """

    def __enter__(self, *args, host="localhost", port=6600, **kwargs):
        try:
            self.connect(host, port)
            LOGGER.info("Successfully connected!")
            return self
        except Exception as e:
            # Being greedy here for now
            LOGGER.error("Failed to connect to MPD", exc_info=True)
            raise Exception("Failed to connect to MPD")

    def __exit__(self, *args, **kwargs):
        LOGGER.info("Shutting down the MPD Client.")
        self.close()
        self.disconnect()
        LOGGER.info("Successfully disconnected, peace out")


def build_config():
    """
    A helper methoed that builds a config for HarmonyGenMopidy.

    Give a number of input questions build out a standard
    HarmonyGenMopidy configuration file.

    .. codeauthor:: Animus Null <sean@animus.design>
    """
    lastfm_user = input("LastFM User Name: ")
    lastfm_apikey = input("LastFM API Key: ")
    lastfm_apisecret = input("LastFM API Secret: ")
    print("During each run a configurable number of songs will\n",
          "be added to the playlist. This next value specifies\n",
          "how many songs will be added on each run.")
    songs_to_add = input("How many songs to add on reach run: ")
    print("This tool works such that it constantly adds tracks.\n",
          "To your playlist. The next question specifies how long it\n"
          "waits between each run. Time is in seconds.")
    time_between_runs = input("How long to wait before adding tracks: ")
    with open(os.path.join(TEMPLATE_DIR, "config.ini"), 'r') as t:
        tmpl = Template(t.read())
    with open(os.path.join(BASE_DIR, "harmonygenmopidy.cfg"), 'w') as w:
        rsp = tmpl.render(lastfm_user=lastfm_user,
                          lastfm_apikey=lastfm_apikey,
                          lastfm_apisecret=lastfm_apisecret,
                          time_between_runs=time_between_runs,
                          songs_to_add=songs_to_add
                          )
        w.write(rsp)


def load_config():
    config_file = os.path.join(BASE_DIR, "harmonygenmopidy.cfg")
    LOGGER.debug("Loading core configuration from: {}.".format(config_file))
    if not os.path.isfile(config_file):
        LOGGER.info("Provided configuration file does not exist.")
        build_config()
    parser = ConfigParser()
    parser.read(os.path.join(BASE_DIR, "harmonygenmopidy.cfg"))
    return parser


def reduce_genres(genres):
    """
    Utility that parses down similar genres.

    Given a list of genres, like:

    *electro swing, electro-swing, Electro Swing, etc.*

    We parse this down using **difflib.SequenceMatcher**.
    This compares two strings, and looks at the numbers
    of edits it takes to match. Calculating a probability.

    We start from the 0 index. Then loop from the 1st
    index to the last. Comparig genres. Removing anything
    that has a 70% or higher match.

    **Note this is returns a new list. It doesn't modify the existing list.**

    .. codeauthor:: Animus Null <sean@animus.design>

    :param genres: A list of genres of a string type.
    :returns: A reduced list of genres.
    """
    reduced_genres = list(genres)
    for i, g in enumerate(reduced_genres):
        for si, sg in enumerate(reduced_genres):
            if i == si  or "delete" in sg or si <= i:
                continue
            sim = SequenceMatcher(None, g, sg).ratio() * 100
            if sim > 70:
                reduced_genres[si] = "delete_{}".format(reduced_genres[si])
    reduced_genres = [g for g in reduced_genres if "delete" not in g]
    return reduced_genres


def get_random_sample(iterator, sample_size):
    """
    Method to provide a random sample set.

    Given an iterator, and a sample size.

    Iterate over the list, generating a random
    index to use from the iterator. Denoting
    the used random index. Ensuring not
    to return repeat elements.

    .. codeauthor:: Animus Null <sean@animus.design>

    :param iterator: A python iterator element.
    :param sample_size: How many elements to pull from the given iterator.
    :returns: An iterator, containing a random selection of X elements. Where x == *sample_size*
    """
    try:
        if len(iterator) <= int(sample_size):
            return list(iterator)
        used_samples = set()
        sample = []
        i = 0
        while i < int(sample_size):
            sample_idx = randint(0, len(iterator) - 1)
            if sample_idx in used_samples:
                while sample_idx in used_samples:
                    sample_idx = randint(0, len(iterator) - 1)
            used_samples.add(sample_idx)
            sample.append(iterator[sample_idx])
            i += 1
        return sample
    except Exception as e:
        LOGGER.error(
            "Encountered exception getting random sample, for {}".format(iterator),
            exc_info=True)
