from musicbrainz.db import Artist, Tag
from harmonygen_mopidy.db import ArtistOrigin
from harmonygen_mopidy.lastfm import LastFM
from sqlalchemy.orm import joinedload_all
from uuid import UUID
import logging
LOGGER = logging.getLogger(__name__)


def get_db_artists(session, artist_list, lastfm_client):
    """
    .. codeauthor:: Animus Null <sean@animus.design>

    :param engine:  A SQLAlchemy engine connection.
    :param artist_list: A List of artist tuple containing name first, and musicbrainz id secondarily.
    :returns: A list of DB artist.
    """
    rsp = []
    objects = []
    for name, mbid in artist_list:
        if mbid.strip() == "" or mbid is None:
            continue
        exists = session.query(Artist).filter(Artist.gid == mbid).first()
        if not exists:
            LOGGER.error(
                "Failed to retrieve artist, this should be in the central knowledge repository..., {}/{}".format(name, mbid))
            continue
        else:
            rsp.append(exists)
    return rsp


def get_db_genres(session, genres, lastfm_client):
    """
    .. codeauthor:: Animus Null <sean@animus.design>

    :param engine:  A SQLAlchemy engine connection.
    :param genres: A list of genres
    :returns: A list of DB artist.
    """
    rsp = []
    objects = []
    for genre in map(lambda x: x.lower(), genres):
        exists = session.query(Genres).options(
            joinedload_all('*')).filter(Genres.name == genre).first()
        if not exists:
            genre = Genres(name=genre, related=[])
            rsp.append(genre)
        else:
            rsp.append(exists)
    session.bulk_save_objects(objects)
    session.commit()
    return rsp


def add_origin_mapping(
        origin_mbid,
        recommended_mbid,
        recommended_name,
        session,
        musicbrainz_session):
    try:
        LOGGER.debug("Checking if artist origin mapping exists in database.")
        try:
            UUID(recommended_mbid)
        except Exception:
            LOGGER.warning("The recommend artist mbid of {} is not a uid.".format(recommended_mbid))
            query = musicbrainz_session.query(Artist).filter(
                Artist.name == recommended_name).first()
            if not query:
                LOGGER.warning(
                    "Could not find artist {} ine musicbrainz databases.".format(recommended_name))
                return
            recommended_mbid = query.gid
        rsp = session.query(ArtistOrigin).filter(
            ArtistOrigin.id == recommended_mbid).one_or_none()
        if not rsp:
            LOGGER.debug("Artist origin mapping is not present in DB.")
            rsp = ArtistOrigin(id=recommended_mbid, origin=[])
            session.add(rsp)
        rsp.origin.append(origin_mbid)
    except Exception as e:
        LOGGER.error(
            "Encountered an error with adding origin mapping.",
            exc_info=True)
        exit()


def get_or_create(session, model, fields, **kwargs):
    instance = session.query(model).filter_by(**kwargs).first()
    if instance:
        return instance
    else:
        data = dict(**fields)
        data.update(**kwargs)
        instance = model(**data)
        session.add(instance)
        session.commit()
        return instance
