"""
.. codeauthor:: Animus Null<sean@animus.design>
"""
from sqlalchemy import (Table, Column, Integer, String,
                        MetaData, ForeignKey, select,
                        DateTime, Boolean, UniqueConstraint,
                        Sequence, Enum, TIMESTAMP)
from sqlalchemy.orm import (sessionmaker, relationship,
                            scoped_session
                            )
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.postgresql import JSONB, ARRAY, UUID
from contextlib import contextmanager
from sqlalchemy import create_engine
import logging
import os
import enum
import logging
LOGGER = logging.getLogger(__name__)


@contextmanager
def session_scope():
    """Provide a transactional scope around a series of operations."""
    session = Session()
    musicbrainz_session = MusicBrainzSession()
    try:
        yield session, musicbrainz_scope
        session.commit()
    except Exception as e:
        LOGGER.error("Error in session context.")
        session.rollback()
        raise
    finally:
        session.close()


@contextmanager
def scoped_sessions():
    """Provide a transactional scope around a series of operations."""
    session = ScopedSession()
    musicbrainz_session = ScopedMusicBrainzSession()
    try:
        yield session, musicbrainz_session
        session.commit()
    except Exception as e:
        LOGGER.error("Error in session context.", exc_info=True)
        session.rollback()
        raise
    finally:
        session.close()


def get_db_connection(
    user="harmonygen",
    password=os.environ.get("dbpassword", None),
    host="animusnull.drunkensailor.org",
    port=5432,
    db="harmonygen"
):
    """
    Obtain a sql alchemy engine connection.

    .. codeauthor:: Animus Null<sean@animus.design>

    :params user: The database user.
    :params password: The database user password.
    :params host: The host to connect to.
    :params db: The database to connect too.
    :returns: A sqlalchemy engine
    """
    if password is None:
        raise KeyError(
            "The database user password was not properly set in the environment.")
    engine = create_engine(
        'postgresql://{user}:{password}@{host}:{port}/{db}'.format(
            user=user,
            password=password,
            host=host,
            port=port,
            db=db),
    )
    return engine


def get_musicbrainz_connection(
    user="musicbrainz",
    password=os.environ.get("musicbrainzpassword", None),
    host="animusnull.drunkensailor.org",
    port=5433,
    db="musicbrainz"
):
    """
    Obtain a sql alchemy engine connection for music brainz.

    .. codeauthor:: Animus Null<sean@animus.design>

    :params user: The database user.
    :params password: The database user password.
    :params host: The host to connect to.
    :params db: The database to connect too.
    :returns: A sqlalchemy engine
    """
    if password is None:
        raise KeyError(
            "The database user password was not properly set in the environment.")
    engine = create_engine(
        'postgresql://{user}:{password}@{host}:{port}/{db}'.format(
            user=user,
            password=password,
            host=host,
            port=port,
            db=db),
    )
    return engine


Base = declarative_base()
Session = sessionmaker(get_db_connection(), autoflush=True)
ScopedSession = scoped_session(Session)
MusicBrainzSession = sessionmaker(get_musicbrainz_connection())
ScopedMusicBrainzSession = scoped_session(MusicBrainzSession)


class ArtistOrigin(Base):
    __tablename__ = "artist_origin"

    id = Column(UUID, nullable=False, unique=True, primary_key=True)
    origin = Column(ARRAY(UUID))


class SimilarArtistService(enum.Enum):
    LastFM = "LastFM"


class SimilarArtist(Base):
    __tablename__ = "similar_artist_cache"

    id = Column(UUID, nullable=False, unique=True, primary_key=True)
    origin_service = Column(Enum(SimilarArtistService))
    first_scanned = Column(TIMESTAMP)
    last_scanned = Column(TIMESTAMP)
    parent_id = Column(UUID, ForeignKey("similar_artist_cache.id"))
    similar = relationship("SimilarArtist")
