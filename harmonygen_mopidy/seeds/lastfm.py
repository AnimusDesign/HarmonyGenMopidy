from harmonygen_mopidy.lastfm import LastFM
from datetime import datetime, timedelta
from random import randint
import json
import logging
LOGGER = logging.getLogger(__name__)


def lastfm_historian(
        config,
        engine,
        lastfm_client,
        sample_periods=2,
        **kwargs):
    """
    All Encompassing history seed.

    This seed will go through your weekly top charts on LastFM.
    Choosing several sample periods. The exact number is defined
    via the keyword argument *sample_periods*.

    We verify to only pull a sample period once.
    Then add those artists to a set.

    This seed is most beneficial if you have a long
    play history, with LastFM. The more backing data
    you have the better.

    By arbitrarily choosing a set of top artists
    in a variety of weeks. We have a higher
    probability of getting a disparte data set.

    .. code_author: Animus Null <sean@animus.design>

    **LastFM APIs Used**

    * `user.getWeeklyChartList <https://www.last.fm/api/show/user.getWeeklyChartList>`
      * Used to gather the available epochs for the weekly charts, see next api.
    * `user.getWeeklyArtistChart <https://www.last.fm/api/show/user.getWeeklyArtistChart>`
      * Given the epochs from the prior query, retrieve that specific weekly Artist Chart.

    :param config: An instance of ConfigParser for *harmonygenmopidy.cfg*)
    :param lastfm_client: An instance of the last fm client.
    :param sample_periods: The total number of backing sample periods to get.
    :returns: A set of strings, which are the artist names.
    """
    availble_chart_times = lastfm_client.user_get_weekly_chart_list(
        {"user": config["LastFM"]["username"]})
    availble_chart_times = availble_chart_times["weeklychartlist"]["chart"]
    i = 0
    used_samples = set()
    sample_artists = set()
    while i < int(sample_periods):
        sample_idx = randint(0, len(availble_chart_times) - 1)
        if sample_idx in used_samples:
            while sample_idx in used_samples:
                sample_idx = randint(0, len(availble_chart_times) - 1)
        used_samples.add(sample_idx)
        weekly_top_charts = lastfm_client.user_get_weekly_artist_chart(
            {
                "user": config["LastFM"]["username"],
                "from": availble_chart_times[sample_idx]["from"],
                "to": availble_chart_times[sample_idx]["to"]})
        for a in weekly_top_charts["weeklyartistchart"]["artist"]:
            sample_artists.add((a["name"], a["mbid"]))
        i += 1
    LOGGER.debug(
        "Loaded {} artists into sample seed.".format(
            len(sample_artists)))
    return sample_artists
