from harmonygen_mopidy.seeds.lastfm import *

AVAILABLE_SEEDS = {
    "LastFMHistorian": {
        "function": lastfm_historian,
        "config_section": "LastFMHistorian",
        "help": "Go through your weekly top charts from lastfm and generate a random seed sample."
    }
}
