from urllib.error import HTTPError
import urllib.request
import urllib.error
import urllib.parse
import urllib.request
import urllib.parse
import urllib.error
import json
import logging
LOGGER = logging.getLogger(__name__)


class LastFM:
    """
    A meta programming function. That dynamically
    builds out a class and corresponding functions
    for the last fm api.

    If the api key is not present in the os
    environment. An exception is thrown.

    .. codeauthor:: Animus Null <sean@animus.design>

    :param config: A dictionary with keys of name, method and required.
    :returns: A parsed json structure into a python dictionary.
    """

    _methods = [
        {
            "name": "artist_getinfo",
            "method": "artist.getinfo",
            "required": ["artist", "api_key"]
        },
        {
            "name": "artist_getinfo_by_mbid",
            "method": "artist.getinfo",
            "required": ["mbid", "api_key"]
        },
        {
            "name": "artist_getsimilar",
            "method": "artist.getSimilar",
            "required": ["artist", "api_key"]
        },
        {
            "name": "artist_getsimilar_by_mbid",
            "method": "artist.getSimilar",
            "required": ["mbid", "api_key"]
        },
        {
            "name": "artist_gettoptracks",
            "method": "artist.getTopTracks",
            "required": ["artist", "api_key"]
        },
        {
            "name": "album_getinfo",
            "method": "album.getInfo",
            "required": ["artist", "album", "api_key"]
        },
        {
            "name": "tags_gettopartists",
            "method": "tag.getTopArtists",
            "required": ["tag", "api_key"]
        },
        {
            "name": "tags_getsimilar",
            "method": "tag.getSimilar",
            "required": ["tag", "api_key"]
        },
        {
            "name": "track_getinfo",
            "method": "track.getInfo",
            "required": ["track", "artist", "api_key"]
        },
        {
            "name": "user_get_weekly_artist_chart",
            "method": "user.getWeeklyArtistChart",
            "required": ["user", "from", "to", "api_key"]
        },
        {
            "name": "user_get_weekly_chart_list",
            "method": "user.getWeeklyChartList",
            "required": ["user", "api_key"]
        }

    ]
    _api_key = None
    _api_url = "http://ws.audioscrobbler.com/2.0"

    def __init__(self, api_key):
        self._api_key = api_key

    @classmethod
    def _build_func(cls, config):
        def _query_base(self, params):
            if not self._api_key:
                raise KeyError("The LastFM API Key has not been set yet.")
            params.update({"api_key": self._api_key,
                           "method": config['method'],
                           "format": "json"
                           })
            url = "%s?%s" % (self._api_url, urllib.parse.urlencode(params))
            try:
                req = urllib.request.urlopen(url)
                data = req.read()
                return json.loads(data.decode('utf-8'))
            except Exception as e:
                LOGGER.error("", exc_info=True)
        return _query_base


for m in LastFM._methods:
    qfunc = LastFM._build_func(m)
    LOGGER.debug("Building function for {}".format(m["name"]))
    setattr(LastFM, m['name'], qfunc)
