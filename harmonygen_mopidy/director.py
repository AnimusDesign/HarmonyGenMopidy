from harmonygen_mopidy import BASE_DIR
from harmonygen_mopidy.utils import (build_config, load_config, reduce_genres,
                                     get_random_sample)
from harmonygen_mopidy.lastfm import LastFM
from harmonygen_mopidy.db import (get_db_connection,
                                  Base, scoped_sessions)
from harmonygen_mopidy.db.utils import get_db_artists
from harmonygen_mopidy.seeds import AVAILABLE_SEEDS
from harmonygen_mopidy.target.helpers import *
from sqlalchemy.exc import IntegrityError
from time import sleep
import logging
import os
LOGGER = logging.getLogger(__name__)
PRINT_EXCEPTIONS = False


def harmonygenmopidy_director(args):
    """
    The core director for HarmonyGenMopidy. essentially start here.

    .. codeauthor Animus Null <sean@animus.design>

    :param args: Instance of argument parser.
    :returns: None
    """
    LOGGER.debug("In HarmonyGen Director")
    if os.path.isdir(BASE_DIR) is False:
        LOGGER.info(
            "Creating the base config directory at: {}".format(BASE_DIR))
        os.makedirs(BASE_DIR)
        LOGGER.info("Bootstrapping base configuration")
    LOGGER.info("Loading core configuration.")
    config = load_config()
    LOGGER.info("Establishing connection to database")
    engine = get_db_connection()
    Base.metadata.create_all(engine)
    lastfm_client = LastFM(config["LastFM"]["api_key"])
    if config["HarmonyGen"]["seed_method"] not in AVAILABLE_SEEDS:
        raise KeyError("Specified origin seed is not in the available list.")
    seed_config = AVAILABLE_SEEDS[config["HarmonyGen"]["seed_method"]]
    seed_function = seed_config["function"]
    if seed_config["config_section"] not in config.sections():
        raise KeyError("Configuration for specified seed is not set.")
    seed = seed_function(
        config,
        engine,
        lastfm_client=lastfm_client,
        **dict(config[seed_config["config_section"]])
    )
    with scoped_sessions() as sessions:
        session, musicbrainz_session = sessions
        artists = get_db_artists(musicbrainz_session, seed, lastfm_client)
    genres = []
    track_add_loop(config, artists, genres, lastfm_client)


def track_add_loop(config, artists, genres, lastfm_client):
    """
    Primary loop to get tracks to add.

    This will execute as an infinite loop. Not dying or
    terminating until explicitly told.

    First we derive a random sample from the seed artists.
    Whatever seed methodology is used. It should return a
    large list of artists.

    Based off the configuration denoted below. We will look
    for *x* artists. These artists are spanned out into similar
    artists. There is an additional configuration. Please note
    the branch depth works such that it counts a total recursion.
    But it will recurse on each object in the similar artist list.

    I.E.

    .. code-block:: python
        :name: Branch Depth Example

        1
                         =============
        =============   /= Similar 2 =
        = Artists 1 = -- =============
        =============   \=============
                         = Similar 1 =
                         =============

        2                                  =================
                         =============   / = Sub Similar 2 =
        =============   /= Similar 2 = --  =================
        = Artists 1 = -- =============
        =============   \=============     =================
                         = Similar 1 = --  = Sub Similar 1 =
                         =============   \ =================       =====================
                                                                   = Sub Sub Similar 2 =
        3                                  =================       =====================
                         =============   / = Sub Similar 2 = ----/
        =============   /= Similar 2 = --  =================     \ =====================
        = Artists 1 = -- =============                             = Sub Sub Similar 2 =
        =============   \=============     =================       =====================
                         = Similar 1 = --  = Sub Similar 1 =
                         =============   \ =================

    .. code-block:: python
        :name: Config Example

        [HarmonyGen]
        songs_to_add = 5
        similar_branch_depth = 3

    The deeper the branch depth. The more fringe artists you'll get. This will also
    tilt the playlist further from the origin period.

    .. todo:: The get random sample logic may not be sufficient.
    .. todo:: Add genre check to ensure sample set is not tilted one way.
    .. todo:: Add weighting to the branch depth selection. I.E. if you set max newness go farther from origin.


    .. seealso:: `class:harmonygen_mopidy.db.Artists`
    .. codeauthor:: Animus Null <sean@animus.design>

    :param ConfigParser config: An instance of config parser for the core configuration.
    :param Artists artists: A list of Artists.
    :param str genres: A list of genre names.
    :param LastFM: An instance of the lastfm client.
    :returns: None
    """
    LOGGER.debug("Beginning Track Loop")
    while True:
        if not config["HarmonyGen"]["songs_to_add"]:
            raise KeyError("The number of songs to be added must be set.")
        if bool(config["HarmonyGen"]["genre_seesaw"]):
            random_sample = genre_seesaw(config, artists)
        else:
            random_sample = get_random_sample(
                list(artists), config["HarmonyGen"]["songs_to_add"])
        LOGGER.debug("Starting seed is {}".format(
            [a.name for a in random_sample]))
        sample_set = {a.gid: [(a.name, a.gid)] for a in random_sample}
        for artist in random_sample:
            LOGGER.debug("Pulled {} - {} from random sample of seed.".format(
                artist.name, artist.gid))
            similar = get_similar_artists_director(
                config,
                artist.gid,
                lastfm_client,
                branch_depth=int(config["HarmonyGen"]["similar_branch_depth"])
            )
            sas = [(sa["name"], sa["mbid"])
                   for sa in similar
                   if "mbid"in sa]
            if artist.gid not in sample_set:
                LOGGER.warning("This should have been added but wasn't")
                sample_set[artist.gid] = []
            sample_set[artist.gid].extend(sas)
        final_selection = {base: get_random_sample(sartists, 1).pop()
                           for base, sartists in sample_set.items()
                           if len(sartists) > 1
                           }
        tracks = {}
        LOGGER.info("Working with artists of {}".format(
            [s[0] for i, s in final_selection.items()]))
        for base, sartist in final_selection.items():
            name, mbid = sartist
            top = [{"artist": name,
                    "artist_mbid": mbid,
                    "track": t["name"],
                    "track_mbid": t["mbid"]}
                   for t in lastfm_client.artist_gettoptracks({"artist": name})["toptracks"]["track"]
                   if "mbid" in t
                   ]
            if len (top) < 1:
                continue
            tracks.update({base: get_random_sample(top, 5)})
        add_tracks(config, tracks, lastfm_client)
        LOGGER.debug(
            "Sleeping for: {} zzzzz".format(
                config["HarmonyGen"]["sleep"]))
        input()
        sleep(int(config["HarmonyGen"]["sleep"]))

from musicbrainz.db import ArtistTag, Tag, TagRelation
from sqlalchemy import desc

def get_artist_genres(artists):
    """
    Helper method to organize artists by genres.

    *In MusicBrainz paralance Tag == Genre*

    Given a list of artists. From the MusicBrainz
    database. Iterate over the artists. Retreiving
    the top genre. That is to say we only gather one
    genre. That one genre is the one that the most
    amount of people has tagged for that artist.

    .. codeauthor:: Animus Null <sean@animus.design>

    :param artists: A list of MusicBrainz Artists.
    :returns: A tuple containing a dictionary and list.
    """
    with scoped_sessions() as sessions:
        session, musicbrainz_session = sessions
        LOGGER.debug("Gathering genres for {} artists.".format(len(artists)))
        artists_by_genre = {}
        genres = []
        for artist in artists:
            if not artist:
                continue
            tags = musicbrainz_session.query(ArtistTag, Tag).filter(ArtistTag.artist==artist.id).order_by(desc(ArtistTag.count)).join(Tag, Tag.id==ArtistTag.tag).first()
            LOGGER.warning("Unable to find tags for artists: {}".format(artist.name))
            if tags is None:
                continue
            artist_tag, tag = tags
            genres.append(tag)
            if tag.name not in artists_by_genre:
                artists_by_genre[tag.name] = []
            artists_by_genre[tag.name].append(artist)
        genres = list(set(genres))
        LOGGER.debug("Retrieved {} genres for given artists.".format(len(genres)))
    return artists_by_genre, genres

def genre_seesaw(config, artists):
    """
    This method given a list of artists. Will organize them by genre/tag then return a random subset of those.
    
    Given a list of artists. We build out a dictionary with the key being the respective genre.
    The helper function is denoted below. We iterate over the list of returned genres.
    Querying the musicbrainz database, we determine the tag relation, between every genre.

    This is a nested loop, where we start from the inital genre index.
    Then slice from that index forward.  If no relation is found between
    those genres, then we continue.

    If a relation is found. We look at the weighted cutoff. This
    is provided via the configuration, a snippet is included below.
    We then build out a dictionary. With the key again being genres.
    But if the genres are similar to another, we join them by a
    comma.

    Finally with the artists split by genre, and pooling similar genres.
    We randomly select several genres from the available list. Then
    select one artist at random from that pool. Building out the
    sample artist selection, that is returned to the track add loop.
    

    .. code-block:: python
        :name: Config Sample

        genre_seesaw = True
        genre_seesaw_weight_cutoff = 300
    
    .. seealso:: `func:harmonygen_mopidy.director.get_artist_genres`

    .. todo:: Use LastFM in case a tag cannot be found for an artist.

    * `Upstream MusicBrainz Schema <https://musicbrainz.org/doc/MusicBrainz_Database/Schema>` 

    :param config: An instance of the ConfigParser.
    :param artists: A list of artists from MusicBrainz.
    :returns: A sub list of artists.
    """
    songs_to_add = int(config["HarmonyGen"]["songs_to_add"])
    with scoped_sessions() as sessions:
        session, musicbrainz_session = sessions
        reduced_genres = []
        new_artists_by_genre = {}
        artists_by_genre, genres = get_artist_genres(artists)
        LOGGER.debug(genres)
        for i, genre in enumerate(genres):
            if genre.id in reduced_genres:
                continue
            siblings = []
            for sgenre in genres[i+1:]:            
                if sgenre.id in reduced_genres:
                    continue
                relation = musicbrainz_session.query(TagRelation).filter(TagRelation.tag1==genre.id).filter(TagRelation.tag2==sgenre.id).one_or_none()
                if not relation:
                    LOGGER.debug("No relation found between {} and {}".format(genre.name, sgenre.name))
                    continue
                if relation.weight >= int(config["HarmonyGen"]["genre_seesaw_weight_cutoff"]):
                    LOGGER.debug("Found sibling genre relation {} -> {} with a weight of {} which is over the cutoff of {}".format(
                        genre.name, sgenre.name, relation.weight, config["HarmonyGen"]["genre_seesaw_weight_cutoff"]))
                    siblings.append(sgenre.name)
                    reduced_genres.extend([genre.id, sgenre.id])
            if len(siblings) > 1: 
                siblings.append(genre.name)
                k = ",".join(siblings)
                if k not in new_artists_by_genre:
                    new_artists_by_genre[k] = []
        excluded_genres = [g.name for g in genres
            if g.name not in reduced_genres]
        LOGGER.debug("Found the following genres that cannot be recuded: {}".format(excluded_genres))
        for e in excluded_genres:
            new_artists_by_genre[e] = artists_by_genre[e]
        for k, v in new_artists_by_genre.items():
            genres = k.split(",")
            for g in genres:
                v.extend(artists_by_genre[g])
        response = []
        if len(new_artists_by_genre.keys()) < songs_to_add:
            LOGGER.debug("The number of songs we wish to add is less than the different genres.")
        else:
            LOGGER.debug("Number of genres is greater than songs to add.")
            selected_genres = get_random_sample(list(new_artists_by_genre.keys()), songs_to_add)
            LOGGER.debug("Final sample of genres to work with is: {}".format(selected_genres))
            for slctg in selected_genres:
                response.extend(get_random_sample(new_artists_by_genre[slctg], 1))
        return response
