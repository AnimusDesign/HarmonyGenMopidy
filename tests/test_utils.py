import pytest
import os
import json
import sys, os
myPath = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, myPath + '/../')

from harmonygen_mopidy.utils import *
from harmonygen_mopidy.lastfm import LastFM

def test_reduce_genres():
    similar_genres = ["electro-swing", "electro swing", "electroswing"]
    print(similar_genres)
    expected = similar_genres[0]
    rsp = reduce_genres(similar_genres)
    assert rsp == ["electro-swing"]
    different_genres = ["metal", "pop", "edm"]
    sorted(different_genres)
    assert reduce_genres(different_genres) == different_genres

def test_get_random_sample():
    # How do you test for randomness?
    sample_numbers = [i for i in range(30)]
    assert get_random_sample(sample_numbers, 2) != get_random_sample(sample_numbers, 2) != get_random_sample(sample_numbers, 2)

def test_lastfm_meta_build():
    expected_methods = [
        "artist_getinfo",
        "artist_getinfo_by_mbid",
        "artist_getsimilar",
        "artist_getsimilar_by_mbid",
        "artist_gettoptracks",
        "album_getinfo",
        "tags_gettopartists",
        "tags_getsimilar",
        "track_getinfo",
        "user_get_weekly_artist_chart",
        "user_get_weekly_chart_list",
    ]
    for em in expected_methods:
        assert hasattr(LastFM, em) is True
