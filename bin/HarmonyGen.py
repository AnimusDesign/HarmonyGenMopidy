#!/usr/bin/env python3
from harmonygen_mopidy.director import harmonygenmopidy_director
from harmonygen_mopidy.utils import build_config
from harmonygen_mopidy import fh
from daemonize import Daemonize
from functools import partial
import argparse
import os

def get_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--daemon", default=True,
        help="Run in background, default: True", action="store_true")
    parser.add_argument("-f", "--foreground", default=False,
        help="Run in the foreground, default: False", action="store_true")
    parser.add_argument("-c", "--config", default=os.path.join(os.environ["HOME"], ".config", "harmonygen_mpd", "harmonygenmopidy.cfg"),
        help="The path to the configuration file.")
    parser.add_argument("-g", "--generate_config", action="store_true",
        help="Generate the base configuration")
    return parser.parse_args()


if __name__ == "__main__":
    args = get_args()
    if args.generate_config:
        build_config()
    if not os.path.isfile(args.config):
        print("The base configuration does not exist.\n",
            "Please See the getting started guide.\n",
            "The run with the '--generate_config' flag.\n",
            "Exiting."
        )
        exit(1)
    if args.foreground:
        print("Starting HarmonyGen in the foreground.")
        harmonygenmopidy_director(args)
    elif args.daemon:
        print("Starting HarmonyGen in the background.")
        daemon_app = partial(harmonygenmopidy_director, args)
        daemon = Daemonize(
            app="HarmonyGen",
            pid="/tmp/HarmonyGen.pid",
            action=daemon_app,
            keep_fds=[fh.stream.fileno()]
        )
        daemon.start()
