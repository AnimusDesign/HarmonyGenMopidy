# Summary

# Summary

An ancillary extension for mopidy. Using common data sets to build out
music recommendations.

* **License**: Apache License

# Goals

* Provide a Nomadic play list generation style over a Hoarder.
* Balance the playlist so it is not tilted to a specific genre.
* Allow for configuration to tweak the playlist as much as possible.
