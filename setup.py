import os
from setuptools import setup, find_packages


setup(
    name="HarmonyGenMopidy",
    version="0.10",
    author="AnimusNull",
    author_email="sean@animus.design",
    description=("Take a bit of the apple of discord to your music collection."),
    include_package_data=True,
    scripts=['bin/HarmonyGen.py'],
    license="Apache",
    keywords="flask, mopidy, mpd",
    packages=find_packages(),
    package_data={
        '': ['*.ini' ]
    },
    install_requires=[
        "flask",
        "daemonize",
        "python-mpd2",
        "sqlalchemy",
        "psycopg2"],
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Utilities",
        "License :: OSI Approved :: Apache License",
    ],
)
