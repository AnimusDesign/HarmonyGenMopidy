Getting Started
===============

Summary
#######

This is alpha level software at this time. It works, but 
is not built for consumption outside of developer use. This
is caused namely by the need for instaniating or running two
PostgreSQL database. One for HarmonyGen, the other for Musicbrainz.

Databases
^^^^^^^^^

**HarmonyGen**

This database will cache similar artists from LastFM. Store an origin mapping.
Providing the ability to trace back, how an artist was initially discovered/added.
This will be automatically created by the python program. Provided authentication
is properly configured.

**MusicBrainz**

MusicBrainz is a very large database. But with that size, you get alot of information.

`I recommend using this docker image <https://github.com/jsturgis/musicbrainz-docker>`

LastFM
^^^^^^

For the time being last fm is central to this. You will need an api account.
In future revisions, the dependency on LastFM will be removed.

`LastFM API <https://www.last.fm/api>`

Secrets
^^^^^^^

Once you have both databases configured. You will need to setup secretes.

.. code-block:: python
    :name: Secrets

    export dbpassword=MAGICDATA
    export musicbrainzpassword=MAGICDATA
    export lastfm_apikey=MAGICDATA
    export lastfm_apisecret=MAGICDATA

Installing
^^^^^^^^^^

`Grab the latest artifact from the development repository. <http://animusnull.drunkensailor.org/nexus/#browse/browse/components:AnimusDesignPython>`

.. code-block:: python
    :name: Pip Install

    pip install PyMusicBrainzDB-1.0-py3-none-any.whl 
    pip install HarmonyGenMopidy-0.10-py3-none-any.whl

Running
^^^^^^^

For the first run, execute this in the fore ground.

.. code-block:: python
    :name: Build Configuration

    HarmonyGen.py -g 

.. code-block:: python
    :name: Run in the foreground
    
    HarmonyGen.py -f

Once you have verified everything is running properly. You
can start in the background as follows.

.. code-block:: python
    :name: Run in the background.
    
    HarmonyGen.py -d


For further debugging. Logs are stored at *~/.config/harmonygen_mpd/harmonygen.log*.
Debug is enabled for the time being.
