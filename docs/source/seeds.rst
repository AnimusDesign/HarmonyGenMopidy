Seeds
=====

Seeds are meant to provide the base foundation
for the playlist. Whether it be looking through
your play history. Finding something you haven't
listened to in a while. Or looking at the last
two weeks, and giving you something new. But
still in line with what you want.

Base
----
.. automodule:: harmonygen_mopidy.seeds
    :members:

LastFM
------
.. automodule:: harmonygen_mopidy.seeds.lastfm
    :members:
