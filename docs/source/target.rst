Target
======

A target is where the recommended song
will end up. Currently only mopidy is supported.
But it is left open and modular for extension.

Base
----
.. automodule:: harmonygen_mopidy.target
    :members:

Helpers
-------
.. automodule:: harmonygen_mopidy.target.helpers
    :members:

Mopidy
------
.. automodule:: harmonygen_mopidy.target.mopidy
    :members:
